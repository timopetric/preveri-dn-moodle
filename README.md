# Preveri status domačih nalog na moodle e-učilnici #

## program uporablja [python 2](https://www.python.org/downloads/ "Download Python 2") ##

### NASTAVITVE ###

1. Najprej v direktoriju odpri terminal in zaženi naslednji ukaz: 

    * ```pip install --user requests && pip install --user bs4```

2. Ustvari datoteko **secrets.py** v katero podaj "user_USERNAME" (moodle uporabniško ime) in "user_PASSWORD" string.

    * Datoteko **secrets.py** shrani v isti direktorij, kot program main.py.

    * ```
username = "user_USERNAME"
password = "user_PASSWORD"
```

### UPORABA ###

Zaženi program tako: ```$ python main.py```

### KAKO DELUJE? ###

Program za vsak dogodek (event) na strani ```MOODLE_URL /calendar/view.php``` preveri, če se na podstrani posameznega dogodka pojavi katerikoli izmed nizov:

* Neoddano

* Neocenjeno

* Oddaj nalogo

* Poskusi kviz zdaj


### PRIMER ###

![alt primer uporabe](images/screenshot.png)



\*Disclaimer: program je v fazi testiranja in **ne zagotavlja** delovanja v vseh primerih. Zaenkrat deluje samo za moodle strani **ucilnica.fri.uni-lj.si**