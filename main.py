# -*- coding: utf-8 -*-
import requests
from requests.adapters import HTTPAdapter
from bs4 import BeautifulSoup



NEODDANO = 	["Neoddano", "Neocenjeno", "Oddaj nalogo", "Poskusi kviz zdaj"]
ODDANO =	[]


"""	printStatus
	@param: imeNaloge = <string> 	the name of the homework
	@param: imePredmet = <string> 	the name of the course
	@param: status = <string> 		status of the assignment
	@param: datum = <string> 		the date if the assignment
"""
def printStatus(imeNaloge, imePredmet, status, datum):
	print imePredmet+": "+imeNaloge
	if status:
		print "\tKONCANO"
	else:
		print "\t! KONCAJ DO: "+datum+" !"
	print ""



"""	getDogodki
	@param: username = <string> mail of the user, used to login to moodle
	@param: password = <string> password of user to login
"""
def getDogodki(username, password):

	LOGIN_DATA = {
		"username": 	username,
		"password": 	password,
		"anchor": 	""
	}

	HEADERS = {
		"Accept": 			"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
		"Accept-Encoding": 		"gzip, deflate, sdch, br",
		"Accept-Language": 		"en-US,en;q=0.8",
		"Cache-Control": 		"max-age=0",
		"Connection": 			"keep-alive",
		"Host": 				"ucilnica.fri.uni-lj.si",
		"Upgrade-Insecure-Requests": 	"1",
		"Content-Type": 		"application/x-www-form-urlencoded",
		"User-Agent": 			"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36"
	}

	# login
	try:
		with requests.Session() as s:
			s.mount("http://", requests.adapters.HTTPAdapter(max_retries=20))	#set max retries to: numMaxRetries
			s.mount("https://", requests.adapters.HTTPAdapter(max_retries=20))	#set max retries to: numMaxRetries
			s.headers.update(HEADERS)	# set headers of the session

			# login to get correct session id (cookies...)
			response = s.post('https://ucilnica.fri.uni-lj.si/login/index.php', timeout=10, data=LOGIN_DATA)

			# get calendar page with events
			response = s.get('https://ucilnica.fri.uni-lj.si/calendar/view.php', timeout=10)
			page = response.text
			if "Niste prijavljeni." in page:
				print "Prijava neuspesna."
				exit()

			soup = BeautifulSoup(page, "html.parser")
			

			listEventov = soup.find_all('div', attrs={'class': 'event'})
			# za vsak <div class=event> na strani:
			for event in  listEventov:

				imeNaloge = event.find('h3', attrs={'class': 'referer'})
				linkNaloge = imeNaloge.find("a")["href"]	# naslov do naloge
				imeNaloge = imeNaloge.text 			# ime naloge

				imePredmet = event.find('div', attrs={'class': 'course'}).text

				datumOddaje = event.find('span', attrs={'class': 'date'}).text

				
				nalogaResponse = s.get(linkNaloge, timeout=10, headers=HEADERS)	# poglej na vsako stran od naloge, ce je ze koncana/oddana
				
				
				#print "\n", imeNaloge, linkNaloge, imePredmet, datumOddaje, "\n"

				# za vsak text v NEODDANO, ki namiguje da naloga ni koncana prever ce res ni koncana
				# TODO: dodej se prevejranje ce je koncana, oz vrni napako/opozorilo ce nc ne najdes
				oddanoBool = True
				for statusText in NEODDANO:
					if statusText in nalogaResponse.text:
						oddanoBool = False


				printStatus(imeNaloge, imePredmet, oddanoBool, datumOddaje)


	except Exception, e:
		print e
		print "Error downloading page."
		exit()



from secrets import username
from secrets import password

getDogodki(username, password)